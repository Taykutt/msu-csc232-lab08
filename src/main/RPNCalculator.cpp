/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file RPNCalculator.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Taylor Kuttenkuler <tay124@live.missouristate.edu>
 *          Dillon Flohr <dillon987@live.missouristate.edu>
 *			Lyubov Sidlinskaya <sidlinskaya1@live.missouristate.edu>
 * @brief   Definition of RPNCalculator.
 */

#include "RPNCalculator.h"
#include <sstream>
#include <iostream>

namespace csc232 {
    int RPNCalculator::evaluate(const std::string &postfixExpress) {
		//Create a string that only has digits and operators
		std::string s = "";
		for(char ch : postfixExpress) {
			if(isOperand(ch)) {
				s = s + ch;
			}
			else {
				switch(ch){	
					case '+': 
						s = s + "+";
						break;
					case '-': 
						s = s + "-";
						break;
					case '*': 
						s = s + "*";
						break;
					case '/': 
						s = s + "/";
						break;
				}
			}
		}
		for(char ch : s) {
			//If character is an operand, turn into string, then put onto stack
			if(isOperand(ch)) {
				int chInt = ch - '0';
				std::string st = std::to_string(chInt);
				stack.push(st);
			}
			else {
				//If character is an operator, get first and second operands on stack, turn into integers
				// then perform approriate function
				int op2 = std::stoi(stack.peek());
				stack.pop();
				int op1 = std::stoi(stack.peek());
				stack.pop();
				switch(ch){
					case '+':
					{
						std::string result = std::to_string(op1 + op2);
						stack.push(result);
						break;
					}
					case '-':
					{
						std::string result = std::to_string(op1 - op2);
						stack.push(result);
						break;
					}
					case '*':
					{
						std::string result = std::to_string(op1 * op2);
						stack.push(result);
						break;
					}
					case '/':
					{
						std::string result = std::to_string(op1 / op2);
						stack.push(result);
						break;	
					}
				}
			}
		}
		return std::stoi(stack.peek());
    } // end RPNCalculator::evaluate(const std::string&)

    bool RPNCalculator::isOperand(const char op) const {
		if (isdigit(op)) {	
			return true;
		}
		else {
			return false;
		}
    } // end RPNCalculator::isOperand(const char) const
} // end namespace csc232