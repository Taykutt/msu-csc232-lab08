/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file RPNCalculator.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Specification of RPNCalculator.
 */

#ifndef LAB08_RPN_CALCULATOR_H
#define LAB08_RPN_CALCULATOR_H

#include <memory>
#include <string>
#include "ArrayStack.h"

namespace csc232 {
    /**
     * Reverse-Polish Notation calculator.
     */
    class RPNCalculator {
    public:
        /**
         * Evaluates a postfix arithmetic expression that contains only integers and the following
         * mathematical operators: + - * /
         * @pre The given expression is a valid, postfix expression with single digit int operands
         * @param postfixExpress the expression to evaluate
         * @return the value computed by the given expression
         */
        int evaluate(const std::string &postfixExpress);
    private:
        /**
         * An array-based stack used to evaluate postfix arithmetic expressions
         */
        ArrayStack<std::string> stack;

        /**
         * A helper operation to support evaluation of postfix expressions
         * @param op a potential operand
         * @return true if the given op is not an operator or white space, false otherwise
         */
        bool isOperand(const char op) const;
    }; // end class RPNCalculator
} // end namespace csc232

#endif //LAB08_RPN_CALCULATOR_H
